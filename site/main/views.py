from django.shortcuts import render, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from main.models import UserData
from django.views.generic.list import ListView
from main.models import Course

from django.contrib.auth.forms import UserCreationForm

#from registration.forms import RegistrationForm
#from registration.models import RegistrationProfile

# Create your views here.
@login_required
def home(request):
    context = RequestContext(request)
    user = request.user
    udata = UserData.objects.get(user = user)

    return render_to_response('main/index.html', {"udata": udata}, context)

def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('main.views.home'))

def user_login(request):
    context = RequestContext(request)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=str(username), password=str(password))

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('main.views.home'))
            else:
                return render_to_response("main/login.html", {"error": "Your account has been deactivated"}, context)
        else:
            return render_to_response("main/login.html", {"error": "Invalid login details supplied."}, context)
    else:
        return render_to_response('main/login.html',{}, context)

def register(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            udata = UserData.objects.create(user = new_user, is_ta = False, is_teacher = False)
            return HttpResponseRedirect("/")

    return render(request, "main/registration.html", {
            'form': form,
    })

#class CourseListView(ListView):
 #   model = course

