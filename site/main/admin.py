from django.contrib import admin
from main.models import UserData, Course

# Register your models here.
admin.site.register(UserData)
admin.site.register(Course)
